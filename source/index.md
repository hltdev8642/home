---
title: Jared Barresi - Home
---

<img style="width:unset; margin:inherit;" src="about/profilepic.png">

### [About](./about)
### [Resume](./resume)
### [Projects](./projects)
### [Tutorials](projects/#Tutorials)
