---
title: Projects
---

## Django
- [Guestbook](../../django)

## Android
- [DeliveryHelper](../2022/03/02/DeliveryHelper)
- [BetaChecker](../2019/12/30/BetaChecker)
- [DroidXTK](../2019/12/29/DroidXTK)

## Bash
- [npm-binary-search](../2021/06/08/npm-binary-search)
- [godaddy-domain-checker](../2021/06/10/godaddy-domain-checker)

## Tutorials
- [Flightgear to Blender to X-Plane](../2022/03/10/fg2bl2xp)

## Other
- [scrcpy](../2022/03/02/Scrcpy)
