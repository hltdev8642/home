---
title: About
---

<img style="width:unset; margin:inherit;" src="./profilepic.png">

**Jared Barresi**
Software Engineer
Boston MA, 02184
781-413-1587
[jaredsbarresi@gmail.com](mailto:jaredsbarresi@gmail.com)


- <a href="https://github.com/hltdev8642"  class="flat-box waves-effect waves-block">Github</b></a>
- <a href="https://gitlab.com/hltdev8642"  class="flat-box waves-effect waves-block">Gitlab</b></a>
- <a href="https://www.linkedin.com/in/jaredbarresi"  class="flat-box waves-effect waves-block">LinkedIn</b></a>

Software Engineer/Programmer graduate from the University of Massachusetts Boston with a Bachelors of Science in Computer Science, and a fundamental knowledge of software and hardware applications. Seeking to utilize a broad background of technical and programming skills to thrive as an entry-level software engineer.
