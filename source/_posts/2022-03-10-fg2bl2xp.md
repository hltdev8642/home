---
title: Converting Flightgear Aircraft to X-Plane
date: 2022-03-10 18:19:30
tags:
---

## Links/Files

<hr>

> <h3 style="margin: 0px 0px;">Pre-configured Workspace (Recommended)</h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[\[-link-\]](https://www.mediafire.com/file/zewo92np2l8x9d5/fg2xptutorial.zip/file)

<hr><h3 style="margin: 0px 0px;">Blender \(2.78\)</h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[\[-link-\]](https://download.blender.org/release/Blender2.78)<h3 style="margin: 0px 0px;">Blender \(latest\)</h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[\[-link-\]](https://www.blender.org/download)
<hr><h3 style="margin: 0px 0px;">fg2xplane</h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[\[-link-\]](https://github.com/www2000/fg2blender)
<hr><h3 style="margin: 0px 0px;">FlightGear Aircraft Downloads</h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>stable releases:</u>
</h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[\[-link-\]](http://mirrors.ibiblio.org/flightgear/ftp/Aircraft)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>unstable releases:</u>
</h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[\[-link-\]](http://mirrors.ibiblio.org/flightgear/ftp/Aircraft-trunk)<h3 style="margin: 0px 0px;">FlightGear Program (optional)</h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[\[-link-\]](https://www.flightgear.org/download)

<hr>

## Videos
> Coming Soon!!!
