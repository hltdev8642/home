---
title: BetaChecker
date: 2019-12-30 18:51:06
tags: [Android, JavaScript, Droidscript, Developer, Apps, Tools, Utilities, APK]
---

<img style="width:unset; margin: auto;" src="https://play-lh.googleusercontent.com/a_tkpyUgWgXLO7a_1-L1mmpIaJB3GTpZRUQTnrcf9Q2WmLCxjHr0LCskLTCoX7Bj-g=s180-rw" >

# Desciption
Lists all of your installed applications, and when you select one provides a link to beta testing program (and opens in Google Play Store) if it exists. Automatically checks off items for apps you have already viewed, to track those beta programs in which you have already looked at.


## Install/Download
[Download APK](https://drive.google.com/file/d/1ZishrnaXS4NX2mzFreIihPNA6RLY50DS/view?usp=sharing)


## Source
[Github](https://gitlab.com/hltdev8642/BetaChecker)
