---
title: godaddy-domain-checker
date: 2021-06-10 17:06:34
tags: [Bash, Shell, Command-Line, Terminal, Utilities]
---

## Description
* Check whether a domain is available or not on godaddy.com.

## Usage
godaddy-domain-checker.sh `[DOMAIN TO QUERY]`


## Development
* Requires a godaddy developer key, which you can get [here](https://developer.godaddy.com/getstarted).
Note: Demo works without a key, as it is set as a hidden environment variable in replit, however if you implement it yourself just get the key and add the followingvariables at the top of the script:

```
API_KEY="[API_KEY]"
SECRET="[API_SECRET]"

```

## Demo
{% iframe https://replit.com/@hltdev8642/godaddy-domain-checker?output=1&embed=1 100% 500px %}

## Source
[Github](https://github.com/hltdev8642/godaddy-domain-checker)
