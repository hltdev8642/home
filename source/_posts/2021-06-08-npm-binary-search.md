---
title: npm-binary-search
date: 2021-06-08 18:48:54
tags: [Javascript, Npm, Nodejs, Shell, Bash, Terminal, Command-Line, Utilities]
---

## Description
Searches npm registry for input query arg ($1) and returns global/binary-containing modules in an organized (sorted) table of results.

## Usage

`npm-binary-search [query]`


## Flags

` -h, --help	Show this help message`

## Config

Config file is generated at `$HOME/.config/npm-bin-search/out_props.config`.
To set properties you want to be printed in output, add them to the `output_props.config` file.

Example file:
 ```
 repository_url
 description
 [etc...]

 ```

## Demo
{% iframe https://replit.com/@hltdev8642/npm-bin-search?output=1&amp;embed=1 100% 500px %}

## Source
[Github](https://github.com/hltdev8642/npm-bin-search)

