---
title: DeliveryHelper
date: 2022-03-02 21:50:06
tags: [Android, JavaScript, Droidscript, Developer, Apps, Tools]
---

<img style="width:unset; margin: auto;" src="https://play-lh.googleusercontent.com/QScb5NBoZkFZz_wQzy8RwbTLEcYY7pH4TGRyMneTnHstmuAxzhrBG3PLiNeNI1lI2VA=s180-rw" >

# Desciption
DeliveryHelper is a utility to assist delivery drivers in making more informed decisions on which orders to either accept or deny.

It is designed and tested by an actual delivery driver, and works well with Uber/UberEats, Doordash, Instacart, and essentially any offer-based delivery driver platform.

Usage:
The DeliveryHelper widget floats on the screen over all your delivery apps, and contains 3 sliders of which include:
&nbsp;&nbsp;&nbsp;&nbsp;(1) Pay Rate: How much you are being offered in dollars. (ex: $12.00)
&nbsp;&nbsp;&nbsp;&nbsp;(2) Distance: How far you will have to travel in mi/km. (ex: 3 mi)
&nbsp;&nbsp;&nbsp;&nbsp;(3) Time: how long the trip is estimated to take in minutes. (ex: 20 min)

Simply enter these 3 values by adjusting the sliders provided, and the widget will quickly calculate for you the projected:
&nbsp;&nbsp;&nbsp;&nbsp;(1) Pay per mi/km: (ex: $2.25/mi)
&nbsp;&nbsp;&nbsp;&nbsp;(2) Pay per hour: (ex: $21.50/hr)

DeliveryHelper enables drivers to make quick calculations within the generally short window of time given to accept orders, and effectively choose the most profitable ones that have your best interest in mind.

Note/Disclaimer:
DeliveryHelper is not affiliated with any of the delivery driving services mentioned (or any others that were not). App is not intended for use while driving.

<a href='https://play.google.com/store/apps/details?id=com.hltdev.deliveryhelper&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img style="max-width:40%" alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png'/></a>


## Source
N/A
